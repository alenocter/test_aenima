<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo $title_for_layout; ?></title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array(		
			'fancybox/jquery.fancybox',
			'base',
			'default', //Siempre el default último
		));
		echo $this->Html->script(array(
			'jquery-1.10.1.min',
			'fancybox/jquery.fancybox',
			'fancybox/jquery.fancybox.pack',
			'default',			
		));

		echo $scripts_for_layout;
		?>
		<script type="text/javascript">
			var APP_BASE = '<?php echo FULL_BASE_URL . Router::url('/') ?>';		
		</script>
	</head>
	<body>
		<?php echo $this->element('ie_warning'); ?>
		<div id="container">
		   <div id="header">
		   		<h1 class="text-center">Test Aenima</h1>
		   </div>
		   <div id="main" class="clearfix">
		   		<?php
				echo $this->Session->flash();
				echo $content_for_layout;
				?>
		   </div>
		   <div id="footer">
				<div class="footer-container clearfix">				
					<p class="developer">Desarrollado por: <a href="http://www.plusglobal.com" target="_blank" title="PlusGlobal"><?php
						 echo $this->Html->image('/img/Logo_PlusGlobal.png', array(
					    'alt' => 'PlusGlobal - desarrollo + aplicaciones web',			    
					)); ?></a></p>
				</div>			
		   </div>
		</div>		
	</body>
</html>