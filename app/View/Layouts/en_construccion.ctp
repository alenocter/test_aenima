<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>En construcción</title>
		<?php
		echo $this->Html->css(array(
			'base',
		));

		echo $scripts_for_layout;
		?>
	</head>
	<body>
		<div id="container" style="background-color: #1f1f1f;">
		   <div id="main" class="clearfix">
		   		<?php
				echo $this->Session->flash();
				echo $content_for_layout;
				?>
		   </div>
		   <div id="footer" style="margin-bottom: 7px;">
				<div class="footer-container clearfix">				
					<p class="developer">Desarrollado por: <a href="http://www.plusglobal.com" target="_blank" title="PlusGlobal"><?php
						 echo $this->Html->image('/img/Logo_PlusGlobal.png', array(
					    'alt' => 'PlusGlobal - desarrollo + aplicaciones web',			    
					)); ?></a></p>
				</div>			
		   </div>
		</div>		
	</body>
</html>