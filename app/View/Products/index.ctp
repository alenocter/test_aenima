<div class="container">
    <div class="well well-sm">
    	<div class="row">
		    <div class="col-xs-12 col-sm-6 col-md-8">
		        <div class="btn-group">
		            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
		            </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
		                class="glyphicon glyphicon-th"></span>Grid</a> 
		        </div>
	        </div>
	       	<div class="col-xs-12 col-sm-6 col-md-4">
		        <form class="bd-search hidden-sm-down">
				  <input class="form-control" id="search-input" placeholder="Search..." autocomplete="off" type="text">
				</form>
			</div>
		</div>
    </div>
    <div id="products" class="row list-group">
        <?php if($products): ?>
			<?php foreach ($products as $key => $product) : ?>	
		        <div class="item col-xs-12 col-sm-6 col-lg-4">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="<?php echo $product['BrwImage']['main']['sizes']['360x350']; ?>" alt="<?php echo $product['Product']['name']; ?>" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading"><?php echo $product['Product']['name']; ?><h4>
		                    <div class="group inner list-group-item-text"><?php echo $product['Product']['description']; ?></div>
		                    <div class="row">
		                        <div class="col-xs-12 col-md-6">
		                            <p class="lead">$<?php echo $product['Product']['price']; ?></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
        	<?php endforeach; ?>
		<?php endif; ?>
    </div>
</div>