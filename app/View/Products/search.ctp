<?php if($products): ?>
	<?php foreach ($products as $key => $product) : ?>	
        <div class="item col-xs-12 col-sm-6 col-lg-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="<?php echo $product['BrwImage']['main']['sizes']['360x350']; ?>" alt="<?php echo $product['Product']['name']; ?>" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading"><?php echo $product['Product']['name']; ?><h4>
                    <div class="group inner list-group-item-text"><?php echo $product['Product']['description']; ?></div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead">$<?php echo $product['Product']['price']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php endforeach; ?>
<?php else: ?>
	<p class="text-center">No se encontraron productos</p>
<?php endif; ?>