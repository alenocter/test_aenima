<?php

class BrwUser extends AppModel {

	public $brwConfig = array(
		'names' => array(
			'plural' => 'Usuarios',
			'singular' => 'Usuario'
		),
		
		'fields' => array(			
			'names' =>array(				
				'password' => 'Contraseña',
				'last_login' => 'Último login',
				'created' => 'Creado',
				'modified' => 'Modificado',
			),
		),		
	);

}