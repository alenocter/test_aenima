<?php

class Product extends AppModel {
	public $name = 'Product';
	public $order = 'Product.sort';
	public $displayField = 'name';

	public $brwConfig = array(
		'names' => array(
			'plural' => 'Productos',
			'singular' => 'Producto',
			'gender' => 1,
		),
		'images' => array(
			'main' => array(
				'name_category' => 'Imagen de listado',
				'sizes' => array('100_100', '350_260', '150x150', '270x270','360x304', '360x350', '810_900',  '1024_1024'),
				'index' => true,
				'description' => false
			),
		),		
		'fields' => array(
			'filter' => array(
				'name',
			),
			'names' => array(
				'name' => 'Nombre',
				'description' => 'Descripción',
				'price' => 'Precio',
				'created' => 'Creado',
				'modified' => 'Modificado',
			),
		),
		'paginate' => array(
			'fields' => array(
				'id', 
				'name', 
				'sort',
			),
			'images' => array('main'),
		),	
	);

	public $brwConfigPerAuthUser = array(
		'FlexiteUser' => array(
			'type' => 'all',
			'brwConfig' => array(
			),
		),
	);

	public function get($id, $design = null) {
		$product = $this->find('first', array(
			'conditions' => array('Product.id' => $id),
			'contain' => array(
				'BrwImage', 
				'BrwFile', 
				'ProductCategory',
			),
		));
		return $product;
	}

}