<div id="header" class="header_login">
	<h1>
		<a href="<?php echo $this->Html->url(array('plugin' => 'brownie', 'controller' => 'brownie', 'action' => 'index', 'brw' => false)) ?>"><i class="fas fa-wrench"></i> <?php echo $companyName ?></a>
	</h1>
</div>
<div id="login">
	<div id="login-inside" class="clearfix">
		<i class="fas fa-unlock-alt"></i>
		<h1><?php echo __d('brownie', 'Login'); ?></h1>
		<p>
			<?php
			echo '<stong>' . __d('brownie', 'Welcome') . '</strong>. ';
			echo __d('brownie', 'Please provide your user and password.');
			?>
		</p>
		<div class="separador"></div>
		<?php
		echo $this->Session->flash('auth');
		echo $this->Form->create();
			echo $this->Form->input('BrwUser.email', array('label' => __d('brownie', 'Email')));
			echo $this->Form->input('BrwUser.password', array('label' => __d('brownie', 'Password')));
			?>
			<div class="submit">
				<button type="submit"><i class="fas fa-check"></i></button>
			</div>
		</form>
	</div>
</div>