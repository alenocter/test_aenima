<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex,nofollow" />
		<link href='http://fonts.googleapis.com/css?family=Asap:400,700' rel='stylesheet' type='text/css'>
		<?php
		echo $this->Html->meta('favicon.ico', Router::url('/favicon.ico'), array('type' => 'icon'));
		echo $this->Html->css(Configure::read('brwSettings.css'));
		echo $this->Html->script(Configure::read('brwSettings.js'));
		?>
		<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
		<script type="text/javascript">
		var APP_BASE = '<?php echo Router::url('/') ?>';
		var SESSION_ID = '<?php //echo $this->Session->id() ?>';
		var BRW_AUTH_USER = <?php echo json_encode(AuthComponent::user()); ?>;
		var brwMsg = {
			no_checked_for_deletion: '<?php echo __d('brownie', 'No records checked for deletion') ?>',
			select: '<?php echo __d('brownie', 'Select') ?>',
			unselect: '<?php echo __d('brownie', 'Unselect') ?>',
			done: '<?php echo __d('brownie', 'Done') ?>',
			show_advanced: '<?php echo __d('brownie', 'Show advanced filters') ?>',
			hide_advanced: '<?php echo __d('brownie', 'Hide advanced filters') ?>',
			hide_menu: '<?php echo __d('brownie', 'Hide menu') ?>',
			show_menu: '<?php echo __d('brownie', 'Show menu') ?>',
		};
		</script>
		<style type="text/css">
		<?php if ($brwHideMenu): ?>
		#menu{display: none;}
		<?php endif; ?>
		</style>
		<title><?php
		echo __d('brownie', 'Admin panel');
		if ($companyName) {
			echo ' - ' . $companyName;
		}
		?></title>
	</head>
	<body>
		<div id="container">
			<?php if (AuthComponent::user('id')) { ?>
				<div id="menu_responsive"></div>
				<div id="menu">
					<div id="header">
						<h1>
							<a href="<?php echo $this->Html->url(array('plugin' => 'brownie', 'controller' => 'brownie', 'action' => 'index', 'brw' => false)) ?>"><i class="fas fa-wrench"></i> <?php echo $companyName ?></a>
						</h1>
					</div>
					<?php echo $this->element('menu') ?>
				</div>
				<div id="options-bar">
					<p id="toggle-menu">
						<?php
						$title = $brwHideMenu ? __d('brownie', 'Show menu') : __d('brownie', 'Hide menu');
						echo $this->Html->link(
							$title,
							array('controller' => 'brownie', 'action' => 'toggle_menu', 'plugin' => 'brownie', 'brw' => false),
							array('title' => $title, 'class' => $brwHideMenu ? 'toggle-hidden' : '')
						);
						?>
					</p>
					<p id="welcome-user"><?php echo __d('brownie', 'User: %s', AuthComponent::user('email')) ?></p>
					<ul>
						<li class="users">
							<?php
							$url = array('controller' => 'contents', 'action' => 'index', 'plugin' => 'brownie', 'brw' => false, AuthComponent::user('model'));
							$anchorText = __d('brownie', 'Users');
							if (AuthComponent::user('model') != 'BrwUser') {
								$url['action'] = 'view';
								$url[] = AuthComponent::user('id');
								$anchorText = __d('brownie', 'User');
							}?>
							<a href="<?php echo $this->Html->url($url);?>"><i class="fas fa-user"></i>&nbsp;&nbsp;<?php echo $anchorText;?></a>
						</li>
						<li class="logout">
							<a href="<?php echo $this->Html->url(array('controller' => 'brownie', 'action' => 'logout', 'plugin' => 'brownie', 'brw' => false));?>"><i class="fas fa-power-off"></i>&nbsp;&nbsp;<?php echo __d('brownie', 'Logout');?></a>
						</li>
					</ul>
				</div>
				<div id="content">
					<?php
					echo $this->Session->flash();
					echo $content_for_layout;
					?>
				</div>
			<?php
			} else {
				echo $this->Session->flash();
				echo $content_for_layout;
			} ?>
		</div>
	</body>
</html>