<?php

Router::connect('/', array('controller' => 'products', 'action' => 'index')); //comentar solamente si el sitio está en construccion
// Router::connect('/', array('controller' => 'pages', 'action' => 'en_construccion')); //descomentar solamente si el sitio está en construccion

//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';