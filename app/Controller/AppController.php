<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {

	var $components = array(
//		'Auth',
		'Session',
		'Brownie.BrwPanel',
		'DebugKit.Toolbar'
	);
	var $helpers = array('Html', 'Js', 'Time', 'Form', 'Session');

	// Este es el menu de Brownie
	var $brwMenu = array(
		'Menú' => array(
			'Productos' => 'Product',
		),
	);

	public function _isPanel() {
		return (!empty($this->params['plugin']) or !empty($this->params['brw']));
	}

	function beforeFilter() {
		if (!$this->_isPanel()) {
		
		}
		//Poner lo que sea dentro de if (!$this->_isPanel()) { y solo llamados a funciones!
    }

	function beforeRender() {
		$this->_setTitle();
	}

	function _setTitle() {
		if (!empty($this->pageTitle)) {
			$this->pageTitle .= ' - ';
		}
		$this->pageTitle .= Configure::read('brwSettings.companyName');
		$this->set('title_for_layout', $this->pageTitle);
	}

}