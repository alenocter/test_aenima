<?php

class ProductsController extends AppController {

	public  $name = 'Products';

	
	public function index($id = null, $slug = null) {
		$this->paginate = array(
			'limit' => (Configure::read('debug')?9999:9999),
			'contain' => array(
				'BrwImage',
			),
		);			
		$this->set(array(
			'products' => $this->paginate('Product'),
		));			
	}
	

	public function search($search = null) {
		$this->layout = 'ajax';
		$conditions = array();
		if(!empty($search)) {
			$conditions = array("OR" => array(
			 	array("Product.name LIKE" => "%".$search."%"),
			 	array("Product.price LIKE" => "%".$search."%"),
			));
		}

		$this->paginate = array(
			'conditions' => $conditions,
			'limit' => (Configure::read('debug')?9999:9999),
			'contain' => array(
				'BrwImage',
			),
		);			
		$this->set(array(
			'products' => $this->paginate('Product'),
		));		
	}

}