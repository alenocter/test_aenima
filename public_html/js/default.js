$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
    search_change();
    search_submit();
});

function search_change() {
	$("#search-input").blur(function(){
		search_ajax();
	});
}
function search_submit() {
    $("form.bd-search").submit(function() {
    	search_ajax();
        return false;
    });
}

function search_ajax() {
	search = $('#search-input').val();
	$.ajax({
		url : APP_BASE + 'products/search/' + search,
		dataType : 'html',
		success : function(data) {
			$('#products').html(data);
		},
		error : function(data) {
			$('#products').html(data);
		},
	});
}